@extends('layout')

@section('content')
    <div id="wrapper" style="color:#0000ff;" class="test">
        <div id="page" class="container">
            <div id="content">
                <div class="title">
                    <h2>Showing all articles</h2>
                <div>
                    @forelse ($articles as $article)
                        <div class="first">
                            <h3><a href="{{ route('articles.show', $article)}}">{{$article->title}}</a></h3>
                            <p>{{$article -> excerpt}}</p>
                            <p>{{$article->body}}</p>

                            {!!$article->excerpt!!}
                        </div>
                    @empty
                        <p>No relevant articles yet.</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection
