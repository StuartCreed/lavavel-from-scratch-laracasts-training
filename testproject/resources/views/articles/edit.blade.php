@extends('layout')

@section('head')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css" rel="stylesheet"/>
@endsection

@section('content')

    <div id="wrapper" style="padding: 20px;">
        <div id="page" class="container">
            <h1>Edit Article</h1>
        </div>

        <form method="POST" action="/articles/{{$article->id}}">
            @method('PUT')
            @csrf

            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="field">
                <label class="label" for="title">Title</label>

                <div class="control">
                    <input class="input @error('title') is-danger @enderror" type="text" name="title" id="title" value="{{$article->title}}">
                    @error('title')
                    <p class="help is-danger">{{$errors->first('title')}}</p>
                    @enderror
                </div>
            </div>

            <div class="field">
                <label class="label" for="excerpt">Excerpt</label>

                <div class="control">
                    <textarea class="textarea @error('excerpt') is-danger @enderror" type="text" name="excerpt" id="excerpt" >{{$article->excerpt}}</textarea>
                    @error('excerpt')
                    <p class="help is-danger">{{$errors->first('excerpt')}}</p>
                    @enderror
                </div>
            </div>

            <div class="field">
                <label class="label" for="body">Body</label>

                <div class="control">
                    <textarea class="textarea @error('body') is-danger @enderror" type="text" name="body" id="body">{{$article->body}}</textarea>
                    @error('body')
                    <p class="help is-danger">{{$errors->first('body')}}</p>
                    @enderror
                </div>
            </div>

            <div class="field is-grouped">
                <div class="control">
                    <button class="button is-link" type="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection
