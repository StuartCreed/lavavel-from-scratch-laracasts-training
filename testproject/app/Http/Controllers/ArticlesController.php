<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Article;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function show(Article $article) {

        //Shows a single resource

        //$article = Article::findOrFail($id);
        //The Article $article does a automatic query for this id built in so it is not necessary.

        return view('articles.show', ['article' => $article]);
    }

    public function index() {

        //Renders a list of a resource

        if (request('tag')) {
            $articles = Tag::where('name',request('tag'))->firstOrFail()->articles;
        }
        else {
            $articles = Article::all();
        }
        return view('articles.index', ['articles' => $articles]);
    }

    public function create() {
        //Shows a view create a resources
        return view('articles.create', [
            'tags' => Tag::all()
        ]);
    }

    public function store() {

        //Persists a new resource

//        $article = new Article;
//        $article->title = request('title');
//        $article->excerpt = request('excerpt');
//        $article->body = request('body');
//        $article->save();
//        THESE LINES OF CODE DO THE SAME AS Article::create

        //Create method creates a new Article and does a save()
//        Article::create([
//            'title' => request('title'),
//            'excerpt' => request('excerpt'),
//            'body' => request('body')
//        ]);

        $this->validateArticle();
        $article = new Article(request(['title', 'excerpt', 'body']));
        $article->user_id = 1; // later this will be auth()->id() once we have finished the chap
        $article->save();
        $article->tags()->attach(request('tags'));

        return redirect(route('articles.index'));

    }

    public function edit(Article $article) {

        return view('articles.edit',['article' => $article]);
        //Show a view to edit an existing resource
    }

    public function update(Article $article) {

//        $article = Article::find($id);
//        $article->title = request('title');
//        $article->excerpt = request('excerpt');
//        $article->body = request('body');
//        $article->save();
//        THESE LINES OF CODE DO THE SAME AS Article->update();

        $article->update($this->validateArticle());

        return redirect('/articles/');
        //Persists the edited resource
    }

    public function destroy() {

        //delete the resource
    }

    /**
     * @return array
     */
    protected function validateArticle()
    {
        return request()->validate([
            'title' => 'required',
            'excerpt' => 'required',
            'body' => 'required',
            'tags' => 'exists:tags,id'
        ]);
    }
}
