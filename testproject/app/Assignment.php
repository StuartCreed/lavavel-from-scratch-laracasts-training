<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    //The below creates a method within the model. This complete
    //method would complete an assignment and change the Database
    //to reflect this.
    public function complete() {
      //The $this command utilises the current entry/row/assignment
      $this->completed = true; //Changes the completed column of this entry to true
      $this->save(); //Commits the change to database.
    }
}
